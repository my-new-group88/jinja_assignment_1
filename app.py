from flask import Flask,render_template
from datetime import datetime, timedelta
from pytz import timezone
import pytz

app=Flask(__name__)

IST = pytz.timezone('Asia/Kolkata')

@app.route('/')
def sample_page():
    now=datetime.now(IST)
    year=now.year
    month=now.month
    day=now.day

    time=now.strftime("%H:%M:%S")
    monthlist=['January','February','March','April','May','June','July','August','September','October','November','December']

    return render_template('index.html',year=year,month=monthlist[month-1],day=day,time=time)




if __name__ == "__main__":
    app.run(debug=True)